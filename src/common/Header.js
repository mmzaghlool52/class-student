import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { teal } from "./Colors";

export default class Header extends Component {
  render() {
    const { backButton, drawer, title, style } = this.props;
    return (
      <View
        style={[
          {
            backgroundColor: teal,
            width: "100%",
            flexDirection: "row",
            elevation: 15,
            justifyContent: "center",
            alignItems: "center"
          },
          {...style},
        ]}
      >
        {backButton && (
          <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.navigation.pop()}>
            <MaterialIcons
              name={"arrow-back"}
              size={25}
              style={{
                color: "white",
                alignSelf: "flex-start",
                margin: 10
              }}
            />
          </TouchableOpacity>
        )}
        {drawer && (
          <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.navigation.toggleDrawer()}>
            <MaterialCommunityIcons
              name={"menu"}
              size={25}
              style={{
                color: "white",
                alignSelf: "flex-start",
                margin: 10
              }}
            />
          </TouchableOpacity>
        )}
        {title ? (
          <Text
            style={{ fontSize: 22, margin: 10, textAlign: "center", flex: 2,  color: "white", flexShrink: 1 }}
          >
            {title}
          </Text>
        ) : (
          <View style={{ flex: 1 }} />
        )}
        <View style={{ flex: 1 }} />
      </View>
    );
  }
}
