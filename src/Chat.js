import React, { Component } from "react";
import {
  FlatList,
  View,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";
import Header from "./common/Header";
import { GiftedChat, Bubble } from 'react-native-gifted-chat'

import { teal } from "./common/Colors";

export default class Chat extends Component {
  state = {
    messages: [],
  }
  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
        },
      ],
    })
  }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  renderBubble (props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: teal
          }
        }}
      />
    )
  }

  render() {
    return (
      <View style={{
        flex: 1
      }}>
        <Header backButton navigation={this.props.navigation} title='Chat' />
        <View style={{
          flex: 1
        }} >

          <GiftedChat
            renderBubble={this.renderBubble}
            messages={this.state.messages}
            onSend={messages => this.onSend(messages)}
            user={{
              _id: 1,
            }}
          />
        </View>
      </View>
    );
  }
}
