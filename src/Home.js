import React, { Component } from "react";
import {
    FlatList,
    View,
    Text,
    Image,
    TouchableOpacity,
} from "react-native";
import Header from "./common/Header"

export default class Home extends Component {
    render() {
        return (
            <View style={{
                flex: 1
            }}>
                <Header drawer navigation={this.props.navigation} title='Home' />
                <View style={{
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <View style={{
                        borderColor: '#008080', // "teal" color
                        borderWidth: 2,
                        width: '90%',
                        height: 120,
                        marginTop: 20,
                        borderRadius: 10,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            color: 'black',
                            fontSize: 50
                        }}>Ads</Text>
                    </View>
                </View>
                <FlatList
                    data={[{ courseName: 'React Native', instructor: 'Marwan Rashed', completed: 50 },
                    { courseName: 'Android', instructor: 'Mostafa Mahmoud', completed: 40 },
                    { courseName: 'Web development', instructor: 'Mahmoud Madkour', completed: 60 },
                    { courseName: 'React Native', instructor: 'Marwan Rashed', completed: 50 },
                    { courseName: 'Android', instructor: 'Mostafa Mahmoud', completed: 40 },
                    { courseName: 'Web development', instructor: 'Mahmoud Madkour', completed: 60 },
                    { courseName: 'React Native', instructor: 'Marwan Rashed', completed: 50 },
                    { courseName: 'Android', instructor: 'Mostafa Mahmoud', completed: 40 },
                    { courseName: 'Web development', instructor: 'Mahmoud Madkour', completed: 60 }]}
                    renderItem={({ item }) => (
                        <View style={{
                            borderColor: '#008080', // "teal" color
                            borderWidth: 1,
                            width: '90%',
                            marginTop: 20,
                            borderRadius: 10,
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            marginLeft: '5%',
                            marginBottom: 5
                        }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{
                                    width:40,
                                    height:40,
                                    borderRadius:20,
                                    borderColor:'#008080',
                                    borderWidth:0.5,
                                    marginLeft:5,
                                    marginRight:5,
                                    marginTop:5
                                }}></View>
                                <View>
                                    <Text style={{color:'black'}}>Course : {item.courseName}</Text>
                                    <Text>Instructor : {item.instructor}</Text>
                                    <Text>Complete : {item.completed} %</Text>
                                </View>
                            </View>
                            <TouchableOpacity style={{width:'100%', marginTop: 5}}>
                            <View style={{
                                backgroundColor:'#008080',
                                width:'100%',
                                borderBottomLeftRadius:10,
                                borderBottomRightRadius:10,
                                alignItems:'center'
                            }}>
                                <Text style={{color:'white'}}>Room Details</Text>
                            </View>
                            </TouchableOpacity>
                        </View>
                    )}
                />
            </View>
        );
    }
}