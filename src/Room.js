import React, { Component } from "react";
import { Dimensions, View, Text, Image, TouchableOpacity } from "react-native";
import Header from "./common/Header";
import Circle from "react-native-progress/Circle";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { StackActions } from "react-navigation";
import { teal } from './common/Colors';

const LANDSCAPE = "LANDSCAPE";
const PORTRAIT = "PORTRAIT";

export default class Room extends Component {
  constructor(props) {
    super(props);
    this.state = {
      announcement:
        "☁ Deploy, manage, and scale cloud applications faster and more efficiently on DigitalOcean. Try DigitalOcean for Free with a $100 Credit. "
    };
  }

  componentWillMount() {
    // getOrientation.
    const { width, height } = Dimensions.get("window");
    const orientation = width > height ? LANDSCAPE : PORTRAIT;
    this.setState({ screenWidth: width, screenHeight: height, orientation });
  }

  componentDidMount() {
    // orientation listener
    Dimensions.addEventListener("change", ({ window: { width, height } }) => {
      const orientation = width > height ? LANDSCAPE : PORTRAIT;
      this.setState({ screenWidth: width, screenHeight: height, orientation });
    });
  }

  componentWillUnmount() {
    // remove orientation listener
    Dimensions.removeEventListener(
      "change",
      ({ window: { width, height } }) => {
        const orientation = width > height ? LANDSCAPE : PORTRAIT;
        this.setState({
          screenWidth: width,
          screenHeight: height,
          orientation
        });
      }
    );
  }

  render() {
    const { announcement, screenWidth, screenHeight, orientation } = this.state;
    return (
      <View
        style={{
          flex: 1
        }}
      >
        <Header drawer navigation={this.props.navigation} title='Your Room' />
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "space-evenly"
          }}
        >
          <Announcements text={announcement} />
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
              alignItems: "center",
              width: "100%",
              margin: 5
            }}
          >
            <CardVert
              icon={require("./images/task.png")}
              title="Assignments"
              screenWidth={screenWidth}
              screenHeight={screenHeight}
              orientation={orientation}
              navigation={this.props.navigation}
            />
            <ProgressCard
              screenWidth={screenWidth}
              screenHeight={screenHeight}
              orientation={orientation}
              navigation={this.props.navigation}
            />
          </View>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
              alignItems: "center",
              width: "100%",
              margin: 5
            }}
          >
            <CardVert
              icon={require("./images/Chat.png")}
              title="Chat"
              screenWidth={screenWidth}
              screenHeight={screenHeight}
              orientation={orientation}
              navigation={this.props.navigation}
            />
            <CardVert
              icon={require("./images/task.png")}
              title="Schedule"
              screenWidth={screenWidth}
              screenHeight={screenHeight}
              orientation={orientation}
              navigation={this.props.navigation}
            />
          </View>
          <CardHorizontal
            icon={require("./images/task.png")}
            title="Questions"
            screenWidth={screenWidth}
            screenHeight={screenHeight}
            orientation={orientation}
            navigation={this.props.navigation}
          />
        </View>
      </View>
    );
  }
}

class CardHorizontal extends Component {
  render() {
    const { title, icon, screenWidth, screenHeight, orientation } = this.props;
    return (
      <View
        style={{
          backgroundColor: "white",
          borderColor: "lightgrey",
          borderWidth: 0.5,
          borderRadius: 7,
          elevation: 5,
          width: screenWidth * 0.9,
          height: screenWidth * 0.25,
          flexDirection: "row",
          justifyContent: "space-evenly",
          alignItems: "center",
          alignSelf: "center"
        }}
      >
        <Image
          source={icon}
          style={{ height: "90%", width: "25%" }}
          resizeMethod="resize"
        />
        <Text
          style={{
            color: teal,
            fontWeight: "500",
            textShadowColor: "grey",
            fontSize: 18
          }}
        >
          {title}
        </Text>
      </View>
    );
  }
}

class CardVert extends Component {
  render() {
    const { title, icon, screenWidth, screenHeight, orientation } = this.props;
    return (
      <TouchableOpacity
        style={{
          backgroundColor: "white",
          borderColor: "lightgrey",
          borderWidth: 0.5,
          borderRadius: 7,
          elevation: 5,
          width: screenWidth * 0.4,
          height: screenWidth * 0.4,
          justifyContent: "space-evenly",
          alignItems: "center"
        }}
        onPress={() => {
          const pushAction = StackActions.push({
            routeName: title,
            params: {
            }
          });

          this.props.navigation.dispatch(pushAction);
        }}
      >
        <Image
          source={icon}
          style={{ height: "70%", width: "70%" }}
          resizeMethod="resize"
        />
        <Text
          style={{
            color: teal,
            fontWeight: "500",
            textShadowColor: "grey",
            fontSize: 18
          }}
        >
          {title}
        </Text>
      </TouchableOpacity>
    );
  }
}

class ProgressCard extends Component {
  render() {
    const { screenWidth, screenHeight, orientation } = this.props;
    return (
      <View
        style={{
          backgroundColor: "white",
          borderColor: "lightgrey",
          borderWidth: 0.5,
          borderRadius: 7,
          elevation: 5,
          width: screenWidth * 0.4,
          height: screenWidth * 0.4,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Circle
          color={teal}
          size={screenWidth * 0.35}
          progress={0.7}
          showsText
          indeterminateAnimationDuration={5000}
        />
      </View>
    );
  }
}

class Announcements extends Component {
  render() {
    const { text } = this.props;
    return (
      <View
        style={{
          backgroundColor: "white",
          borderColor: "lightgrey",
          borderWidth: 0.5,
          borderRadius: 7,
          elevation: 5,
          width: "90%",
          margin: 5,
          flexDirection: "row",
          alignSelf: "center",
          flexShrink: 1,
          padding: 5
        }}
      >
        <Text
          style={{
            color: teal,
            fontWeight: "500",
            textShadowColor: "grey",
            fontSize: 14,
            textAlign: "center"
          }}
        >
          {text}
        </Text>
      </View>
    );
  }
}
