import React, { Component } from "react";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
} from "react-native"

export default class Login extends Component {
    render() {
        return (
            <View style={{
                backgroundColor: '#008080',
                height: '100%',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <View style={{
                    backgroundColor: 'white',
                    width: '90%',
                    height: 50,
                    borderRadius: 10,
                    marginBottom: 10,
                    justifyContent: 'center'
                }}>
                    <TextInput
                        placeholder='Email ...'
                        placeholderTextColor='black'
                        autoCapitalize={"none"}
                        style={{
                            fontSize: 18,
                            color: 'black',
                            flex: 0.8,
                            marginLeft: 10
                        }}
                    />
                </View>
                <View style={{
                    backgroundColor: 'white',
                    width: '90%',
                    height: 50,
                    borderRadius: 10,
                    marginBottom: 10,
                    justifyContent: 'center'
                }}>
                    <TextInput
                        placeholder='Password ...'
                        placeholderTextColor='black'
                        secureTextEntry={true}
                        style={{
                            fontSize: 18,
                            color: 'black',
                            flex: 0.8,
                            marginLeft: 10
                        }}
                    />
                </View>
                <View style={{
                    flexDirection: 'row-reverse',
                    width: '90%'
                }}>
                    <TouchableOpacity style={{
                        marginBottom: 10
                    }}
                    onPress={() => this.props.navigation.navigate('ResetPassword')}
                    >
                        <Text style={{
                            color: 'white',
                            fontSize: 15
                        }}>Forget Password ?</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={{
                    backgroundColor: 'white',
                    height: 50,
                    width: '30%',
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
                    onPress={() => this.props.navigation.navigate('Home')}
                >
                    <Text style={{
                        color: 'black',
                        fontSize: 20
                    }}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{
                    marginTop: 10
                }}
                    onPress={() => this.props.navigation.navigate('Registration')}
                >
                    <Text style={{
                        color: 'white',
                        fontSize: 15
                    }}>Don't have an account !</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
