import {
  createAppContainer,
  createDrawerNavigator,
  createStackNavigator
} from "react-navigation"
import Home from "./src/Home"
import Login from "./src/Login"
import Registration from "./src/Registration"
import ResetPassword from "./src/ResetPassword"
import Room from "./src/Room"
import Chat from "./src/Chat"

const HomeNavigator = createStackNavigator(
  {
    Login: {
      screen: Login
    },
    Registration: {
      screen: Registration
    },
    ResetPassword: {
      screen: ResetPassword
    },
    Drawer: {
      screen: createDrawerNavigator({
        Home: {
          screen: Home
        },
        Room: {
          screen: Room
        },
      }
      )
    },
    Chat: {
      screen: Chat
    },
  },
  {
    headerMode: 'none'
  }
);

export default createAppContainer(HomeNavigator);